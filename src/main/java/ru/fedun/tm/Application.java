package ru.fedun.tm;

import ru.fedun.tm.model.TerminalCommand;
import ru.fedun.tm.utils.NumberUtil;

import java.text.NumberFormat;
import java.util.Scanner;

import static ru.fedun.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        if (args.length != 0) {
            runWithArgs(args[0]);
        }
        runWithCommand();
    }

    private static void runWithCommand() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (true) {
            command = scanner.nextLine();
            switch (command){
                case CMD_VERSION:
                    displayVersion();
                    break;
                case CMD_ABOUT:
                    displayAbout();
                    break;
                case CMD_HELP:
                    displayHelp();
                    break;
                case CMD_INFO:
                    displayInfo();
                    break;
                case CMD_EXIT:
                    exit();
                default:
                    displayError();
            }
        }
    }

    private static void runWithArgs(final String arg) {
        if (arg == null || arg.isEmpty()) {
            System.out.println("Empty command");
            return;
        }
        switch (arg) {
            case ARG_HELP:
                displayHelp();
                break;
            case ARG_ABOUT:
                displayAbout();
                break;
            case ARG_VERSION:
                displayVersion();
                break;
            case ARG_INFO:
                displayInfo();
                break;
            default:
                displayError();
        }
    }

    private static void displayWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println();
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.INFO);
        System.out.println(TerminalCommand.EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.8");
        System.out.println();
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Alexander Fedun");
        System.out.println("sasha171998@gmail.com");
        System.out.println();
    }

    private static void displayInfo(){
        System.out.println("[INFO]");

        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory: " + usedMemoryFormat);

        System.out.println();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayError() {
        System.out.println("Error: unknown command");
        System.out.println();
    }

}
