package ru.fedun.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String ARG_HELP = "-h";

    String CMD_VERSION = "version";

    String ARG_VERSION = "-v";

    String CMD_ABOUT = "about";

    String ARG_ABOUT = "-a";

    String CMD_INFO = "info";

    String ARG_INFO = "-i";

    String CMD_EXIT = "exit";

}
